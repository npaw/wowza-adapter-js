var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Wowza = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getCurrentTime() / 1000
  },

  /** Override to return Frames Per Second (FPS) */
  getFramesPerSecond: function () {
    return this.fps
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.droppedFrames
  },

  /** Override to return video duration */
  getDuration: function () {
    return (this.player.getDuration() / 1000) || null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return isNaN(this.bitrate) ? null : this.bitrate
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    return this.throughput
  },

  /** Override to return rendition */
  getRendition: function () {
    return youbora.Util.buildRenditionString(this.getBitrate())
  },

  /** Override to return title */
  getTitle: function () {
    return this.player.c.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.player.isLive()
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.c.sourceURL
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.c.version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return "Wowza"
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    this.references = []
    this.references['play'] = this.playListener.bind(this)
    this.references['pause'] = this.pauseListener.bind(this)
    this.references['resume'] = this.playingListener.bind(this)
    this.references['completed'] = this.endedListener.bind(this)
    this.references['error'] = this.errorListener.bind(this)
    this.references['playheadTime'] = this.timeupdateListener.bind(this)
    this.references['stats'] = this.statsListener.bind(this)
    this.references['seeked'] = this.seekedListener.bind(this)

    this.player.onPlay(this.references['play'])
    this.player.onPause(this.references['pause'])
    this.player.onResume(this.references['resume'])
    this.player.onCompleted(this.references['completed'])
    this.player.onError(this.references['error'])
    this.player.onPlayheadTime(this.references['playheadTime'])
    this.player.onStats(this.references['stats'])
    this.player.onSeek(this.references['seeked'])
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    this.monitor.stop()

    this.player.removeOnPlay(this.references['play'])
    this.player.removeOnPause(this.references['pause'])
    this.player.removeOnResume(this.references['resume'])
    this.player.removeOnCompleted(this.references['completed'])
    this.player.removeOnError(this.references['error'])
    this.player.removeOnPlayheadTime(this.references['playheadTime'])
    this.player.removeOnStats(this.references['stats'])
    this.player.removeOnSeek(this.references['seeked'])
  },

  /** Listener for 'onPlay' event. */
  playListener: function (e) {
    this.fps = e.player.abrDataCollector.fixedFramesPerSecond
    this.fireStart()
  },

  /** Listener for 'onPlayheadTime' event. */
  timeupdateListener: function (e) {
    if (e.time > 600) {
      this.fireStart()
      this.fireJoin()
      this.lastPlayhead = e.time / 1000
    }
  },

  /** Listener for 'onPause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'onResume' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireJoin()
  },

  /** Listener for 'onSeek' event. */
  seekedListener: function (e) {
    this.fireSeekBegin()
    this.fireSeekEnd()
  },

  /** Listener for 'onError' event. */
  errorListener: function (e) {
    this.fireError("Failure", e.error.message)
  },

  /** Listener for 'onCompleted' event. */
  endedListener: function (e) {
    this.fireStop({ 'playhead': this.lastPlayhead })
  },

  /** Listener for 'onStats' event. */
  statsListener: function (e) {
    this.bitrate = e.bitrate
    this.droppedFrames = e.totalDroppedFrames
    this.throughput = e.throughput
  }
})

module.exports = youbora.adapters.Wowza
